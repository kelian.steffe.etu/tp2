export default class Component {
  tagName;
  attribute;
  children;
  constructor(tagName, attribute, children) {
    this.tagName = tagName;
    this.attribute = attribute;
    this.children = children;
  }
  render() {
    return (this.children && this.attribute ?
      `<${this.tagName} ${this.attribute.name}="${this.attribute.value}">${this.children}</${this.tagName}>` :
      (this.children ?
        `<${this.tagName}>${this.children}</${this.tagName}>` :
        (this.attribute ?
          `<${this.tagName} ${this.attribute.name}="${this.attribute.value}" />` :
          `<${this.tagName} />`
        )
      )
    );
  }
}
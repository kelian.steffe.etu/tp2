import Component from "./Component.js";
export default class Img extends Component {
  link;
  constructor(link) {
    super('img', {name:'src', value:link});
  }
  render() {
    const h1 = new Component('h1', null, 'La Carte');
    return `${h1.render()}\n${super.render()}`;
  }
}
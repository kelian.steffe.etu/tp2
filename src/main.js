import Img from "./components/Img.js";
// import { data } from "./data";

// Render tests for Components

// const title = new Component( 'h1', null, 'La carte' );
// const emptyImg = new Component( 'img' );
// const simpleImg = new Component( 'img', {name:'src', value:'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'} );
// const textImg = new Component( 'img', {name:'src', value:'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'}, 'Text' );

// console.log(title.render() 
// + `\n` + emptyImg.render()
// + `\n` + simpleImg.render()
// + `\n` + textImg.render());

const img = new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300');
console.log(img.render());
document.querySelector('.pageTitle').innerHTML = img.render();